package es.unileon.procesadores.symbols;

/*
 * Autores: ALCAL� VALERA, DANIEL; CRESPO TORBADO, BEATRIZ; HERRERAS G�MEZ, FRANCISCO
 * Pr�ctica: GESTI�N DE TIPOS
 */

import java.util.*;

public class Symbol {
	String nombre;
	Stack tipo;
	
	public Symbol(String nombre, Stack tipo) {
		this.nombre=nombre;
		this.tipo=tipo;
	}
	
	public static String tipotoString(Stack s) {
		String cadena="";
		for (int cont = s.size() - 1; cont >= 0; cont--) {
			switch(((Character)s.elementAt(cont)).charValue()) {
				case('i'): { cadena += "un entero"; break;}
				case('r'): { cadena += "un real"; break;}
				case('c'): { cadena += "un caracter"; break;}
				case('a'): { cadena += "un array de dimensi�n "; cont--; cadena += "" + s.elementAt(cont) + " "; break;}
				case('b'): { cadena += "un boolean"; break;}
				case('f'): { cadena += "una funci�n que retorna "; break;}
				case('u'): { cadena += "indefinido"; break;}
				case('p'): { cadena += "puntero a ";break;}
			}
		}
		
		return cadena;	
	}
	

}
